Assignment1:

A Jenkins Setup has been provisioned for the Demo of the given usecase:
http://35.230.80.113:8080/blue/organizations/jenkins/Assignment1-CICD/activity

Jenkins Credentials: 
User: admin / Password: admin@098

Pipeline: Assignment1-CICD

BlueOceanView:
![BlueOceanView](https://github.com/fluidjeel/mb-interview/blob/main/assignment1/BlueOcean.png)

ParameterView:
![Parameters](https://github.com/fluidjeel/mb-interview/blob/main/assignment1/Parameters.PNG)


