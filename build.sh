#!/bin/bash
sudo chown -R jenkins:jenkins /opt
for c in 1 2 3 4
do
 filenames=$(jq --arg c "$c" '.["Stage\($c)"].Name' builds.json)
 filenames=$(sed -e 's/^"//' -e 's/"$//' <<<"$filenames")
 content=$(jq --arg c "$c" '.["Stage\($c)"].Content' builds.json)
 touch $filenames.txt
 echo $content > filenames.txt
done
rm -rf builds builds.zip
mkdir builds
cp *.txt builds/
ls -ltr builds/
zip -r builds.zip builds
cp builds.zip /opt/
rm -rf builds
